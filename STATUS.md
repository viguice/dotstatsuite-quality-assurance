[[_TOC_]]

## source code tests

types:
- SAST: **veracode** static analysis
- SCA: **veracode** software composition analysis (dependencies)

#### JS

|status|repository|type|trigger|frequency|report|
|---|---|---|---|---|---|
|:interrobang:|[DE](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[DE](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[DV](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[DV](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-viewer)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[DLM](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[DLM](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[SFS](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[SFS](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[SHARE](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-share)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[SHARE](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-share)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[CONFIG](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[CONFIG](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[PROXY](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy)|SAST|scheduled|daily, 3am|?|
|:interrobang:|[PROXY](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy)|SCA|scheduled|daily, 3am|?|
|:interrobang:|[KEYCLOAK](https://gitlab.com/sis-cc/.stat-suite/keycloak)|-|-|-|-|
|:interrobang:|[DATA](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config-data)|-|-|-|-|

#### .NET
*TODO*

## env related tests

types:
- vDAST: **veracode** dynamic analysis
- iDAST: **invicti** dynamic analysis

envs:
- SISCC (devops) staging: not tested
- OECD prod: not tested
- SISCC (devops) qa: tested
- OECD preprod: tested

#### JS

questions:
- are we able to test all apps (ie DE, DV, DLM) regarding the url limitation?
- should services like share or search be also tested?

|status|app/svc|type|trigger|frequency|report|
|---|---|---|---|---|---|
|:interrobang:|DE - https://de-qa.siscc.org|vDAST|scheduled|daily, 3am|?|
|:interrobang:|DE - https://de-qa.siscc.org|iDAST|scheduled|daily, 4am|?|

#### .NET
*TODO*

## misc

statuses:
- :interrobang: not setup
- :heavy_check_mark: setup
- :bangbang: broken
