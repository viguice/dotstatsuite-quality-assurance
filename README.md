[[_TOC_]]

----------

# Purpose
The purpose of this repository is to centralize the tools used for the quality-assurance of the components of the .stat-suite. 
- Currently the quality-assurance covers:
	- **Core Performance testing** 
	- **Security testing**
  - **Functional/Integration testing**
  - **End-to-end tests**
  - **DE browser performance**
  - **Code quality**
  - **Client-side accessibility**

---

# Use cases
The objective of this section is to gather real use cases reported by any member of the SIS-CC community, or at large from anyone using the .Stat SUite in a Production environment, and that would require to work on increasing the performance and/or quality of the application(s).  
Use cases are reported [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/quality/test_cases).

---

# .Stat Core Performance testing
The .Stat Core performance tests are configured in the GitLab CI pipelines, to automatically test the nsiws and transfer services during the development process.
- These tests include performance testing for imports and extractons of data and structures.
> A detailed information about the performance tests can be found [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md).
- There are two pipelines configured to run two performance tests
	- **Triggered jobs** 
	- **Daily scheduled jobs**

## .Stat Core: performance results
The **public performance test result** dashboard is accessible [here](https://siscc.grafana.net/public-dashboards/f98021df13744e65a21168df8c99e597?orgId=1).

The DevOps performance monitoring dashboard is accessible [here](https://siscc.grafana.net/d/cfea3e4d-80a9-47c1-bff4-8b4e12359a64/stat-suite-core-performance-results-devops?orgId=1) to team members only.

**New! metodology:** performance test results dashboard of tests using a GitLab runnner on a dedicated machine.  
The tests are ran mutiple times before each release in order to gather the more consistent performance statistics results. The average of each test is then published in the dashboard below to compensate the variances between each single test run. The variance of the results can come from different reasons:
- Except for smoke tests, all other types of tests randomly choose queries (exports) and files (imports). This means that, if more queries with larger results and files with larger contents are randomly chosen, then the overall performance will be slower, compared to a test ran where the majority were small contents/results.
- Smoke tests use exactly the same test cases (queries and files), in the exact same order and number or requests. But even in this case, when there are no randomness related to the test cases, there is still variance. This variance can come from the machine running the services, the database, or even the programming language execution.

**Infrastructure characteristics:**
- A single machine is used to host all the services (topology) as described in the [dotstatsuite-docker-compose](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose).
- All services run with Docker
- Digitalocean droplet CPU-Optimized machine
- 8 vCPUs
- Memory: 16GB
- SSD: 100 GB

**Core services configurations:**
- Transfer service:
  - Archived: NO
  - KeepHistory: NO 
  - Log level: NOTICE
  - Auth enabled: YES
- NSIWS:
  - Archived: NO
  - KeepHistory: NO 
  - Log level: WARN
  - HTTP response compression enabled: NO
  - Auth enabled: YES



**Previous methodology:** performance test results running on a local machine with the following characteristics:
- CPU: Intel Core i7 - 6 cores
- RAM: 16Go DDR4
- DISK: SDD

<details><summary>Previous methodology results</summary>

| .Stat Suite .NET | [v6.0.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/34) | [v6.1.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/37) | [v6.4.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/41) | [v8.0.1](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/51) | [v8.1.2](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/53) | [almond](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/55) | [blueberry](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/57) | [dragonfruit](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/59) | [elote](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/70) |
|---------------------------------|-------------------|-------------------|-----------------------|-----------------------|-----------------------|-----------------------|---------------------|---------------------|---------------------|
| Release date | Mar-21 | Apr-21 | May-21 | Mar-22 | May-22 | Aug-22 | Nov-22 | Jul-23 | Aug-24 |
| nsiws | - | - | v8.2.0 | v8.9.2 | v8.9.2 | v8.9.2 | v8.12.2 | v8.17.0 | v8.18.2 |
| transfer | v6.0.0 | v6.1.0 | - | v8.0.1 - v9.0.1 | v8.1.2 - v9.1.2 | - | v11.0.7 | v12.0.1 | **v13.0.0** |
| | | | | | | | | | |
| **Smoke-test data imports** | 85.71% ✓ 6 ✗ 1 | 85.71% ✓ 6 ✗ 1 | - | 71.42% ✓ 5 ✗ 2 | 100.00% ✓ 13 ✗ 0 | - | 100.00% ✓ 13 ✗ 0 | 100.00% ✓ 13 ✗ 0 | 100.00% ✓ 13 ✗ 0 |
| - data_import_time | 13.13s | 10.14s | - | 12.68s | 24.21s | - | 20.73s | 13.31s | **15.72s** |
| - datasetSize:extraSmall | 2.42s | 6.13s | - | 2.19s | 2.37s | - | 1.5s | 1.14s | **1.02s** |
| - datasetSize:small | 23.85s | 18.17s | - | 28.43s | 25.48s | - | 27.84s | 16.63s | **18.55** |
| - datasetSize:large | - | - | - | 38.14s | - | - | 29.99s | 19.76s | **24.1s** |
| **Smoke-test data extractions** | - | - | 99.41% ✓ 342 ✗ 2 | 100.00% ✓ 160 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 |
| - http_req_duration | - | - | 311.59ms | 304.54ms | 269.97ms | 304.54ms | 400.56ms | 221.76ms | **240.76ms** |
| - datasetSize:extraSmall | - | - | 222.77ms | 260.97ms | 219.58ms | 260.97ms | 208.65ms | 179.3ms | **166.9ms** |
| - datasetSize:small | - | - | 238.1ms | 244.05ms | 209.68ms | 244.05ms | 224.02ms | 171.31ms | **169.19ms** |
| **Load-test data extractions** | - | - | 93.58% ✓ 9971 ✗ 684 | 96.97% ✓ 11398 ✗ 356 | 92.81% ✓ 9303 ✗ 720 | 96.97% ✓ 11398 ✗ 356 | 99.90% ✓ 10256 ✗ 10 | 100.00% ✓ 15678 ✗ 0 | 100.00% ✓ 12838 ✗ 0 |
| - http_req_duration | - | - | 2.46s | 2.12s | 2.67s | 2.12s | 2.58s | 1.33s | **1.85s** |
| - datasetSize:extraSmall | - | - | 2.21s | 2.40s | 2.71s | 2.4s | 2.17s | 1.2s | **1.65s** |
| - datasetSize:small | - | - | 2.19s | 1.93s | 2.41s | 1.93s | 2.12s | 1.13s | **1.54s** |
| **Stress-test data extractions** | - | - | 95.67% ✓ 8516 ✗ 385 | 85.74% ✓ 10428 ✗ 1734 | 97.91% ✓ 9464 ✗ 202 | 85.74% ✓ 10428 ✗ 1734 | 100.00% ✓ 10249 ✗ 0 | 100.00% ✓ 15613 ✗ 0 | 100.00% ✓ 11468 ✗ 0 |
| - http_req_duration | - | - | 4.15s | 2.78s | 3.74s | 2.78s | 3.47s | 1.93s | **2.99s** |
| - datasetSize:extraSmall | - | - | 3.51s | 2.67s | 3.65s | 2.67s | 2.87s | 1.77s | **2.61s** |
| - datasetSize:small | - | - | 3.91s | 2.47s | 3.5s | 2.47s | 3.02s | 1.69s | **2.62s** |
| - datasetSize:small_paginated | - | - | 3.47s | 1.63s | 2.74s | 1.63s | 3.17s | 1.76s | **2.72s** |
| **Spike-test data extractions** | - | - | 72.12% ✓ 2921 ✗ 1129 | 70.74% ✓ 3516 ✗ 1454 | 68.14% ✓ 3480  ✗ 1627 | 70.74% ✓ 3516 ✗ 1454 | 99.01% ✓ 5214  ✗ 52 | 99.21% ✓ 6064 ✗ 48 | 98.86% ✓ 5148 ✗ 59 |
| - http_req_duration | - | - | 6.39s | 5.19s | 4.79s | 5.19s | 4.64s | 3.82s | **4.74s** |
| - datasetSize:extraSmall | - | - | 6.59s | 5.41s | 5.54s | 5.41s | 3.82s | 3.67s | **4.28s** |
| - datasetSize:small | - | - | 6.06s | 5.06s | 4.52s | 5.06s | 4.3s | 3.36s | **4.17s** |
| - datasetSize:small_paginated | - | - | 6.13s | 4.27s | 3.82s | 4.27s | 3.69s | 3.04s | **3.61s** |
| **Soak-test data extractions** | - | - | - | 100.00% ✓ 42590 ✗ 0 | 100.00% ✓ 46873 ✗ 0 | - | - | - | - |
| - http_req_duration | - | - | - | 1.34s | 1.13s | - | - | - | - |

</details>

---

# Security tests
*accessible to team members only*

- GitLab integrated security scans for DAST, SAST, container, dependency: [link](https://gitlab.com/groups/sis-cc/-/security/vulnerabilities)
- Docker images static scanning: [link](https://hub.docker.com/orgs/siscc/repositories)

---

# Functional/Integration tests
Integration tests of the .Stat Core services ensure the functional quality acceptance of .Stat Suite API features by testing the interaction between each other.

Integration tests are created and stored in **Postman**: see more about **[How to create, run and monitor automated tests in Postman](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/blob/master/recipes/Automated-tests-in-postman.md?ref_type=heads#how-to-create-run-and-monitor-automated-tests-postman-gitlab)**

Integrations tests are using *SDMX* structure and data files stored in GitLab and publicly reusable: see [Resources](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/FunctionalTests/Resources?ref_type=heads)

Integration tests are added to the **CI pipelines** to be run locally or by automatic push: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/ci-templates/postman.yaml?ref_type=heads

---

# end-to-end tests
*To be completed...*

End-to-end *e2e* tests and integrated to the CI pipelines to be run (manually or automatically) against any DevOps or Live environment.

---

# DE browser performances
Using the GitLab integrated feature of the [Sitespeed tool](https://docs.gitlab.com/ee/ci/testing/browser_performance_testing.html), it measures the rendering performance of the DE web pages.  
The CI pipelines are enhanced with the "browser_performance" task to run against the live environment (post-deploy).

- [CI pipeline template](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/ci-templates/fe.yaml?ref_type=heads)

*Current limitation:* only the homepage of DE is tested since it's the only URL that works without knowing the config or the data.

---

# Code quality
Using the GitLab integrated feature of the [Code Climate tool](https://docs.gitlab.com/ee/ci/testing/code_quality.html), Code Quality analyses the source code’s quality and complexity.  
The CI pipelines are enhanced with the "code_quality" task in the `test` stage to run before committing to the `dev` branch.

- [CI pipeline template](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/ci-templates/fe.yaml?ref_type=heads)

*Current limitation:* configuration to refine to avoid false positive and exclude scripts or build files.

---

# Accessibility
Using the GitLab integrated feature of the [Pa11y tool](https://docs.gitlab.com/ee/ci/testing/accessibility_testing.html), it analyzes a defined set of web pages and reports accessibility violations, warnings, or notices if any, based on the [WCAG 2.1 guidelines](https://www.w3.org/TR/WCAG21/#new-features-in-wcag-2-1).  
The CI pipelines are enhanced with the accessibility "a11y" task to run against the live environment (post-deploy).

- [CI pipeline template](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/ci-templates/fe.yaml?ref_type=heads)

*Current limitation:* only the homepage of DE is tested since it's the only URL that works without knowing the config or the data.

---

# CI pipeline setup
## Pipeline triggers
The Quality assurance CI pipeline gets triggered by either when a commit is done on the develop branch in one of the .Stat-suite component repositories or as a scheduled job. 
More details for the Performance tests is documented separatly [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md). 

##  Performance tests
The triggered performance test jobs contain light performance tests for imports and exports of data and structures.
- The jobs are configured to be triggered by a merge of a feature branch into development in the [dotstatsuite-core-transfer repository](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer) 
- The objective of this tests is to:
	- Provide a sanity check every time there are new changes to the NSI-WS and the transfer-service.
	- Verify that these systems doesn't throw any errors when under minimal load.
- The file **.smoke-performance-test-gitlab-ci** describes the steps to be executed.
    - The tests are run against the "qa:reset" space of the [nsi-ws QA environment](http://nsi-reset-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

### Scheduled CI jobs 
The scheduled jobs contain extensive performance tests for imports and exports of data and structures. 
- They are configured to run every weekend to avoid affecting the users, by the high traffic caused by the performance tests.
- These tests can be launched manually, if needed, from the [list of scheduled pipelines](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/pipeline_schedules)
- The objective of this tests is to:
	- Assess the current performance of the NSI-WS and the transfer-service under typical and peak load.
	- Make sure that these systems are continuously meeting the performance standards as changes are made to the system (code).
- The file **.extended-performance-test-gitlab-ci.yml** describes the steps to be executed.
    - The tests are run against the "qa:stable" space of the [nsi-ws QA environment](http://nsi-stable-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

### Manual pipeline run
*With the necessary members permission*, Tests defined in the CI pipelines can be manually triggered manually.

**Step 1:** go to https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/pipelines and click **'Run pipeline'**

**Step 2:**
- Keep the default 'Master' branch of the quality-assurance project (where the official latest version of the tests is stored)
- Put '0' in front of the tests that you don't want to run ('1' by default for all tests to run)
- Use the **`DOCKER_COMPOSE_BRANCH`** Variable to choose the version/release for which you want to run the tests:
  - by default 'develop' to test DevOps-QA env.
  - 'master' to test DevOps-Staging env.

Click 'Run pipeline'.

![Manual pipeline run](/static/images/tests-manual-pipeline-run.png)
