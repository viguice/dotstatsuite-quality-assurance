/******************
	This test provies scenario for spike testing data imports to the transfer-service:
		1.- Spike test is a variation of a stress test, but it does not gradually increase the load, instead it spikes to extreme load over a very short window of time
		2.- While a stress test allows the SUT (System Under Test) to gradually scale up its infrastructure, a spike test does not.
		3.- Determine how your system will perform under a sudden surge of traffic.
		4.- Determine if your system will recover once the traffic has subsided.
	SUCCESS/FAILUER
		Excellent: system performance is not degraded during the surge of traffic. Response time is similar during low traffic and high traffic.
		Good: Response time is slower, but the system does not produce any errors. All requests are handled.
		Poor: System produces errors during the surge of traffic, but recovers to normal after the traffic subsides.
		Bad: System crashes, and does not recover after the traffic has subsided.
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-data-imports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '10s', target: 1 }, // below normal load
		{ duration: '1m', target: 2 },
		{ duration: '10s', target: 7 }, // spike to 140 users
		{ duration: '3m', target: 7 }, // stay at 140 for 3 minutes
		{ duration: '10s', target: 1 }, // scale down. Recovery stage.
		{ duration: '4m', target: 1 }, // continue at 1 users to collect pending imports that are still being processed
	],
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
		"data_import_completed": ['rate>0.90'], // more than 90% success rate of transactions imported
        "data_import_time{import_type:csv_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_medium}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_large}": ["avg<35000"],//less than 35 seconds
        "data_import_time{import_type:xml_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:xml_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:xml_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:sdmx_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:sdmx_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:sdmx_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:excel_extraSmall}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:excel_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:excel_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:excel_large}": ["avg<150000"],//less than 150 seconds
		
        "data_import_time{datasetSize:extraSmall}": ["avg<150000"],
        "data_import_time{datasetSize:small}": ["avg<150000"],
        "data_import_time{datasetSize:medium}": ["avg<150000"],
        "data_import_time{datasetSize:large}": ["avg<150000"],
        "data_import_time{datasetSize:extraLarge}": ["avg<150000"],	
	},
	//iterations: TEST_CASES.length,
	//vus: TEST_CASES.length,

};

export function setup() {
	
	return initConfig(false);
}

export default function(config) {
	
	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
	
	importData(config, testCase, importRate, importTrend);
}