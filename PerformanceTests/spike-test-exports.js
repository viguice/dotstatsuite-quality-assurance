/******************
	This test provies scenario for spike testing a NSI-WS:
		1.- Spike test is a variation of a stress test, but it does not gradually increase the load, instead it spikes to extreme load over a very short window of time
		2.- While a stress test allows the SUT (System Under Test) to gradually scale up its infrastructure, a spike test does not.
		3.- Determine how your system will perform under a sudden surge of traffic.
		4.- Determine if your system will recover once the traffic has subsided.
	SUCCESS/FAILUER
		Excellent: system performance is not degraded during the surge of traffic. Response time is similar during low traffic and high traffic.
		Good: Response time is slower, but the system does not produce any errors. All requests are handled.
		Poor: System produces errors during the surge of traffic, but recovers to normal after the traffic subsides.
		Bad: System crashes, and does not recover after the traffic has subsided.
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { initConfig, exportData } from './Resources/utils.js';

let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-exports.json";
	
//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
export let options = {
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '10s', target: 10 }, // below normal load
		{ duration: '1m', target: 10 },
		{ duration: '10s', target: 140 }, // spike to 140 users
		{ duration: '3m', target: 140 }, // stay at 140 for 3 minutes
		{ duration: '10s', target: 10 }, // scale down. Recovery stage.
		{ duration: '3m', target: 10 },
		{ duration: '10s', target: 0 },
	],
	
	thresholds: {
		'checks': ['rate>0.98'], // more than 98% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}":   	["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": 	["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": 	["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}":       	["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}":  	["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}":["avg<5000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}":       	["avg<60000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}":  	["avg<60000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["avg<5000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}":   	["avg<5000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}":["avg<5000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}":   	["avg<5000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": 	["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": 		["avg<20000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": 	["avg<40000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": 	["avg<30000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": 		["avg<35000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["avg<40000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": 	["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": 	["avg<35000"],
		
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall_paginated}": 	["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size small_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium_paginated}": 		["avg<15000"],
        "http_req_duration{group:::Query type data::Format xml::Size large_paginated}": 		["avg<27000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge_paginated}": 	["avg<30000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall_paginated}":   ["avg<25000"],
        "http_req_duration{group:::Query type data::Format json::Size small_paginated}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format json::Size medium_paginated}": 	    ["avg<30000"],
        "http_req_duration{group:::Query type data::Format json::Size large_paginated}": 		["avg<27000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge_paginated}":   ["avg<25000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall_paginated}": 	["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_paginated}": 		["avg<25000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium_paginated}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format csv::Size large_paginated}": 		["avg<30000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge_paginated}": 	["avg<30000"],
		
        "http_req_duration{datasetSize:extraSmall}":["avg<30000"],
        "http_req_duration{datasetSize:small}": 	["avg<30000"],
        "http_req_duration{datasetSize:medium}": 	["avg<35000"],
        "http_req_duration{datasetSize:large}": 	["avg<40000"],
        "http_req_duration{datasetSize:extraLarge}":["avg<65000"],
		
        "http_req_duration{datasetSize:extraSmall_paginated}":["avg<30000"],
        "http_req_duration{datasetSize:small_paginated}": 	  ["avg<30000"],
        "http_req_duration{datasetSize:medium_paginated}": 	  ["avg<35000"],
        "http_req_duration{datasetSize:large_paginated}": 	  ["avg<40000"],
        "http_req_duration{datasetSize:extraLarge_paginated}":["avg<65000"],	
	},
		
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	return initConfig(true);
}

export default function(config) {

	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    
	exportData(config, testQuery, true);
}
