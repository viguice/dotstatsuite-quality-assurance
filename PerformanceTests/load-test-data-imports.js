/******************
	This test provies scenario for load testing data imports to the transfer-service:
		1.- Assess the current performance of the transfer-service under typical and peak load.
		2.- Make sure that the transfer-service is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';


let PID = __ENV.K6_PID || 3497348;
let TEST_NAME = __ENV.TEST_NAME || "load-test-data-imports";	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-data-imports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	stages: [
		{ duration: '1m', target: 2 }, // simulate ramp-up of traffic from 0 to 2 users over 2 minutes.
		{ duration: '3m', target: 2 }, // stay at 2 users for 3 minutes
		{ duration: '1m', target: 4 }, // ramp-up to 4 users over 1 minutes (peak hour starts)
		{ duration: '1m', target: 4 }, // stay at 4 users for short amount of time (peak hour)
		{ duration: '1m', target: 2 }, // ramp-down to 2 users over 1 minutes (peak hour ends)
		{ duration: '3m', target: 2 }, // continue at 2 for additional 3 minutes
		{ duration: '1m', target: 1 }, // ramp-down to 1 users
		{ duration: '4m', target: 1 }, // continue at 1 users to collect pending imports that are still being processed
	],
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
		"data_import_completed": ['rate>0.60'], // more than 60% success rate of transactions imported
        "data_import_time{import_type:csv_small}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_medium}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:csv_large}": ["avg<35000"],//less than 35 seconds
        "data_import_time{import_type:xml_small}": ["avg<30000"],//less than 20 seconds
        "data_import_time{import_type:xml_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:xml_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:sdmx_small}": ["avg<30000"],//less than 20 seconds
        "data_import_time{import_type:sdmx_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:sdmx_large}": ["avg<150000"],//less than 150 seconds
        "data_import_time{import_type:excel_extraSmall}": ["avg<30000"],//less than 30 seconds
        "data_import_time{import_type:excel_small}": ["avg<30000"],//less than 20 seconds
        "data_import_time{import_type:excel_medium}": ["avg<70000"],//less than  70 seconds
        "data_import_time{import_type:excel_large}": ["avg<150000"],//less than 150 seconds
		
        "data_import_time{datasetSize:extraSmall}": ["avg<150000"],
        "data_import_time{datasetSize:small}": ["avg<150000"],
        "data_import_time{datasetSize:medium}": ["avg<150000"],
        "data_import_time{datasetSize:large}": ["avg<150000"],
        "data_import_time{datasetSize:extraLarge}": ["avg<150000"],	
	},
	//iterations: TEST_CASES.length,
	//vus: TEST_CASES.length,

};

export function setup() {
	
	return initConfig(false);
}

export default function(config) {

	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
	
	importData(config, testCase, importRate, importTrend);
}