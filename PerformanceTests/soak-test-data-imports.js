/******************
	This test provies scenario for soak testing data imports to the transfer-service:
		1.- The soak test uncovers performance and reliability issues stemming from a system being under pressure for an extended period.
		2.- Reliability issues typically relate to bugs, memory leaks, insufficient storage quotas, incorrect configuration or infrastructure failures. 
		3.- Performance issues typically relate to incorrect database tuning, memory leaks, resource leaks or a large amount of data.
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig, importData } from './Resources/utils.js';
	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-data-imports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
//Open input files
for(let testCase in TEST_CASES){
	if(TEST_CASES[testCase].format !=="sdmx")
		TEST_CASES[testCase].data = open(`./Resources/Data/${TEST_CASES[testCase].dataFile}`, "b");
	if(TEST_CASES[testCase].format ==="excel")
		TEST_CASES[testCase].edd = open(`./Resources/Data/${TEST_CASES[testCase].eddFile}`, "b");
}

let importRate = new Rate('data_import_completed');
let importTrend = new Trend('data_import_time', true);

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	//Target = number of max users to scale to
	stages: [
		{ duration: '2m', target: 3 }, // ramp up to 3 users
		{ duration: '40m', target: 3 }, // stay at 3 for ~40 min
		{ duration: '10m', target: 1 }, // continue at 1 users to collect pending imports that are still being processed
	],
	//iterations: TEST_CASES.length,
	//vus: TEST_CASES.length,

};

export function setup() {
	return initConfig(false);
}

export default function(config) {

	TryToGetNewAccessToken(config);
	
	let testCase = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
	
	importData(config, testCase, importRate, importTrend);
}
