/******************
	This test provies scenario for smoke testing data imports to the nsi-ws:
		1.- Assess the current performance of the nsi-ws for basic benchmark tests.
		2.- Make sure that the nsi-ws is continuously meeting the performance standards as changes are made to the system (code).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';
import { Rate, Trend } from 'k6/metrics';
import { TryToGetNewAccessToken, initConfig } from './Resources/utils.js';
	
let BASE_URL = __ENV.NSIWS_HOSTNAME || "http://127.0.0.1:81";	
let INPUT_FILE = __ENV.TEST_CASES_FILE || "./Resources/test-cases-structure-imports.json";

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));

//Open input files
for(let testCase in TEST_CASES){
	TEST_CASES[testCase].File = open(`./Resources/Structures/${TEST_CASES[testCase].structureFile}`, "b");
}

export let options = {
	setupTimeout: "10s",
	systemTags: ['check','error_code','group','method','name','status'],
	thresholds: {
		"checks": ['rate>0.99'], // more than 99% success rate on import requests
        "http_req_duration{group:::Structure type multiple}": ["p(95)<50000"],			
        "http_req_duration{group:::Structure type agencyscheme}": ["p(95)<30000"],
        "http_req_duration{group:::Structure type categoryscheme}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type categorisation}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type codelist}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type conceptscheme}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type contentconstraint}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type dataflow}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type datastructure}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type hierarchicalcodelist}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type metadataflow}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type metadatastructure}": ["p(95)<30000"],	
        "http_req_duration{group:::Structure type structureset}": ["p(95)<30000"],			
	},
	iterations: 1,
	vus: 1
};

export function setup() {
	
	return initConfig(true);
}

export default function(config) {
			
	//Get new access token if current token has expired
	TryToGetNewAccessToken(config);
		
	let headers= {
		'Accept':'*/*',
		'Authorization': `Bearer ${config.accessToken}`, 
		'Content-Type': 'application/xml'
	};
	
	//Submit data import requests
	for(let testCase in TEST_CASES){
		
		console.log(`Importing file: ${TEST_CASES[testCase].structureFile}`);	
		
		group(`Structure type ${TEST_CASES[testCase].structureType}`, function (){
			var url=`${BASE_URL}/rest/structure`;
			var data= TEST_CASES[testCase].File;
			var res = http.post(url, data, {headers: headers});

			console.log(`import status:${res.status}`);
			
			check(res, {
				'is status 201(Created) or 207(Multi-status)': (r) => r.status === 201||r.status === 207
			});
		});
		
		//wait 1 second after each test iteration
		sleep(1);
	}	
}