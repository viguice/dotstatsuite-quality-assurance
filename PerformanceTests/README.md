Contents
---------

[[_TOC_]]

----------

# Performance tests

The purpose of the performance tests is to establish the framework for the automatic performance tests for the .Stat Suite, as part of the DevOps process. This framework allows to detect performance degradations produced by new changes introduced to the source code of the underlying services. The tests focus on two main areas: data imports (transfer-service) and data extractions (nsi-ws).   

## Folder structure
```bash
.
├── Resources
│   ├── Data
│   	└── #Data files to be imported by the transfer-service, indicated in the file test-cases-data-imports.json
│   ├── Structures
│   	└── #SDMX structure files to be imported by the NSI-WS, indicated in the file test-cases-structure-imports.json 
│   ├── test-cases-data-imports.json #List of test cases to be use by the data imports tests.
│   ├── test-cases-exports.json #List of NSI-WS queries to export data or structures, to be used by the exports tests.
│   └── test-cases-structure-imports.json #List of test cases to be use by the structure imports tests.
├── load-test-data-imports.js
├── load-test-exports.js
├── smoke-test-data-imports.js
├── smoke-test-exports.js
├── smoke-test-structure-imports.js
├── soak-test-exports.js
├── spike-test-exports.js
├── stress-test-exports.js
└── README.md
```

## Tools
The performance tests use the open source tool called [K6](https://k6.io/), based on the previous project [load impact](https://loadimpact.com/). 
- K6 can be installed in a local machine or as a docker container to allow anyone to run these tests locally. The results can be seen in the command window.
- The automatic tests are run on docker containers triggered by events in the GitLab CI pipelines. The results are sent to the K6's cloud service.

## Test types 
[Read more in the k6 documentation](https://k6.io/docs/test-types/introduction)
- **[Smoke Test's](https://k6.io/docs/test-types/smoke-testing)** role is to verify that your System can handle minimal load, without any problems.
- **[Load Test](https://k6.io/docs/test-types/load-testing)** is primarily concerned with assessing the performance of your system in terms of concurrent users or requests per second.
- **[Stress Test](https://k6.io/docs/test-types/stress-testing)** and *[Spike testing](https://k6.io/docs/test-types/stress-testing#spike-testing)* are concerned with assessing the limits of your system and stability under extreme conditions.
- **[Soak Test](https://k6.io/docs/test-types/soak-testing)** tells you about reliability and performance of your system over the extended period of time.

## Performance objectives
Performance objectives are collected here from sis-cc members and stakeholders of the .Stat Suite in order to contribute to the performance tests, baselines and reports.

- **For any data queries** (any arbitrary filter selection settings) for up to 3000 observations (number of observations is limited to the range 0 to 2999 through the http range header option): **300 ms**.

- **For structure queries** (dataflow request with all references and `detail=referencepartial`) for up to 3000 ItemScheme items in total: **300 ms**.  
**Note**: The performance target is set for structures that do not exceed a total number of categories, concepts and codes within the response of 3000 items.

- **Import data**
  - Import data (**insert**, **update** and/or **delete**) in **SDMX-CSV format** should take less than **30 sec per 1 million observations** for up to 100 million observations, thus representing 50 min for 100 million observations.
  - This example import [data for 1 country](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/Resources/Data/OECD.ECO-EO_LIVE-1.0-800k.csv.zip) should take less than 16 seconds to upload. 

  - **Objectives per test case:**
  
| File | Format | Test Group | Observations | CSV                 | XML   | EXCEL  |
|------|--------|------------|--------------|---------------------|-------|--------|
|      |        |            |              | 30s per 1 Million obs  | 30s per 1 Million obs                   | 30s per 1 Million obs   |
| PT-WPI-1.0.0-data.csv                                              | CSV    | small      | 19,536       | < 30s        |       |       |
| PT-DF_CPI-1.0.0-data.csv.zip                                       | CSV    | medium     | 654,142      | < 30s        |       |       |
| PT-EO_LIVE-1.0-1_country.csv.zip                                   | CSV    | medium     | 135,916      | < 30s        |       |       |
| PT-EO_LIVE-1.0-1_million_1.csv.zip                                 | CSV    | large      | 1,123,445    | < 33s        |       |       |
| PT-EO_LIVE-1.0-1_million_2.csv.zip                                 | CSV    | large      | 1,020,521    | < 30.6s      |       |       |
| PT-EO_LIVE-1.0-1_million_3.csv.zip                                 | CSV    | large      | 1,040,113    | < 31.2s      |       |       |
| PT-EO_LIVE-1.0-1_million_4.csv.zip                                 | CSV    | large      | 1,081,947    | < 32.4s      |       |       |
| PT-EO_LIVE-1.0-800k.csv.zip                                        | CSV    | large      | 893,370      | < 16s (OECD-ECO goal)|    |       |
| PT-DF_WPI-1.0.0-data.xml                                           | XML    | small      | 19,536       |              | < 30s |       |
| https://nsi-qa-stable.siscc.org/rest/data/PT,DF_WPI,1.0.0/....../  | XML    | small      | 19,536       |              | < 30s |       |
| PT-DF_CPI-1.0.0-data.xml.zip                                       | XML    | medium     | 654,142      |              | < 30s |       |
| https://nsi-qa-stable.siscc.org/rest/data/PT,DF_CPI,1.0.0/......./ | XML    | medium     | 654,142      |              | < 30s |       |
| PT,MILLED_RICE,1.xlsx                                              | EXCEL  | extraSmall | 103          |              |       | < 30s |

  - **Smoke tests thresholds imports**
  
| Test group       | Thresholds |           |
|------------------|------------|-----------|
|                  | Previous   | Current   |
| csv_small        | 10s        |   10s     |
| csv_medium       | 70s        | [- 40s -] |
| csv_large        | 150s       | [- 60s -] |
| xml_small        | 10s        | 10s       |
| xml_medium       | 70s        | 70s       |
| xml_large        | 150s       | 150s      |
| sdmx_small       | 10s        | [+ 30s +] |
| sdmx_medium      | 70s        | 70s       |
| sdmx_large       | 150s       | 150s      |
| excel_extraSmall | 5s         | [+ 10s +] |
| excel_small      | 10s        | 10s       |
| excel_medium     | 70s        | 70s       |
| excel_large      | 150s       | 150s      |

- **Export data** 
  - Data extract *(HTTPS-based SDMX web service calls)* of unfiltered/complete dataflow (with a max of 100 million observations) in **SDMX-CSV format** should take less than **20 ms per 1000 observations**, thus representing 20 sec for 1 million observations and 34 min for 100 million observations.
  - Data extract of a filtered dataflow (with a max of 3000 data points) in **SDMX-JSON format** when only including dimension and time filters and first page, should take less than **50 ms per 1000 observations**, thus representing 150 ms for 3000 observations. When also including advanced parameters such as updatedAfter (later), pagination or lastNObservations, it should take less than **100 ms per 1000 observations**, thus representing 300 ms for 3000 observations.
  - Data extract of a filtered dataflow (with a max of 1 million data points) in **SDMX-CSV format** when only including dimension and time filters, should take less than **50 ms per 1000 observations**, thus representing 5 sec for 100,000 observations and 50 secs for 1 million observations. When including advanced parameters such as updatedAfter (later), lastNObservations, or pagination, it should take less than **100 ms per 1000 observations**, thus representing 10 sec for 100,000 observations and 100 sec for 1 million observations.
  - Data extract of a filtered dataflow (with a max of 1 million data points) in **SDMX-JSON format** and **SDMX-ML format** when only including dimension and time filters, should take less than **100 ms per 1000 observations**, thus representing 10 sec for 100,000 observations and 100 secs for 1 million observations. When including advanced parameters such as updatedAfter (later), lastNObservations, or pagination, it should take less than **200 ms per 1000 observations**, thus representing 20 sec for 100,000 observations and 200 sec for 1 million observations.
  - The following example queries with frequencies 'A' and 'Q' should together take less than 10 seconds per country. 
    - /rest/data/OECD.ECO,EO_LIVE,1.0/FRA..A/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions
    - /rest/data/OECD.ECO,EO_LIVE,1.0/FRA..Q/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions 
    - /rest/data/OECD.ECO,EO_LIVE,1.0/USA..A/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions    
    - /rest/data/OECD.ECO,EO_LIVE,1.0/USA..Q/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions
	
  - **Objectives per test case:**
  
| Query | Number of observations returned by the query | Test Group | CSV non-paginated | CSV  paginated     | JSON/XML non-paginated | JSON/XML paginated | PIPELINE THRESHOLDS  |
|-------|----------------------------------------------|------------|-------------------|--------------------|------------------------|--------------------|----------------------|
|       |                                              |            | 50 ms per 1000obs | 100 ms per 1000 obs| 100 ms per 1000 obs    | 200 ms per 1000 obs|                      |
| PT,DF_CPI,1.0.0/1.10001.10.50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                      | 5     | extraSmall | < 300 ms       | < 300 ms   | < 300 ms       | < 300 ms        |                                 Adjusted from -> to |
| PT,DF_CPI,1.0.0/2.10001.10.50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                      | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |          |
| PT,DF_CPI,1.0.0/3.10001.10.50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                      | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.1.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.2.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.3.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.4.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.5.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.6.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.7.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.8.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/3.10001.10.50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                      | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_WPI,1.0.0/1.THRPEB.1.TOT.10.3.Q/PT?startPeriod=2017-Q2&endPeriod=2018-Q2              | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_WPI,1.0.0/2.THRPEB.1.TOT.10.3.Q/PT?startPeriod=2017-Q2&endPeriod=2018-Q2              | 5                                   | extraSmall | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.10001...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 198                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.20001...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.30002...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 198                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.40005...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.40006...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.40007...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.30003...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 198                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.114120...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                          | 198                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.114121...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                          | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.114122...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                          | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.20002...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/.20003...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 213                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_WPI,1.0.0/...TOT.10.3.Q/PT?startPeriod=2012-Q2&endPeriod=2018-Q2                      | 225                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_WPI,1.0.0/...TOT.10.2.Q/PT?startPeriod=2012-Q2&endPeriod=2018-Q2                      | 225                                 | small      | < 300 ms       | < 300 ms        | < 300 ms        | < 300 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                       | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.1.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.2.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.3.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.4.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.5.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.6.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.7.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1+2+3..10.8.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1..10.1+2+3.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1..10.3+4+5.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_CPI,1.0.0/1..10.6+7+8.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                        | 2,325                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 460 ms        |
| PT,DF_WPI,1.0.0/.THRPEB.....Q/PT?startPeriod=2015-Q2&endPeriod=2018-Q2                      | 2,886                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 580 ms        |
| PT,DF_WPI,1.0.0/.THRPEB.....Q/PT?startPeriod=2010-Q2&endPeriod=2013-Q2                      | 2,886                               | medium     | < 300 ms       | < 300 ms        | < 300 ms        | < 580 ms        |
| PT,DF_CPI,1.0.0/...1+2+3.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...2+3+4.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...3+4+5.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...4+5+6.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...5+6+7.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...6+7+8.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                           | 10,170                              | large      | < 500 ms       | < 1s            | < 1 s           | < 2 s           |
| PT,DF_CPI,1.0.0/...7+8+50.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2                          | 11,736                              | large      | < 580 ms       | < 1.16 s        | < 1.16 s        | < 2.32 s        |
| PT,DF_WPI,1.0.0/all/PT?startPeriod=2000&endPeriod=2018                                      | 16,872                              | large      | < 840 ms       | < 1.68 s        | < 1.68 s        | < 3.36 s        |
| PT,DF_WPI,1.0.0/all/PT?startPeriod=1999&endPeriod=2017                                      | 16,872                              | large      | < 840 ms       | < 1.68 s        | < 1.68 s        | < 3.36 s        |
| PT,DF_CPI,1.0.0/all/?startPeriod=2010                                                       | 191,139                             | extraLarge | < 9.5 s        | < 19 s          | < 19 s          | < 38 s          |
| PT,EO_LIVE,1.0/FRA..A/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions | 27,096                              | extraLarge | < 1.35 s       | < 2.7 s         | < 2.7 s         | < 5.4 s         |
| PT,EO_LIVE,1.0/FRA..Q/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions | 108,820                             | extraLarge | < 5.44 s       | < 10.8 s        | < 10.8 s        | < 21.7 s        |
| PT,EO_LIVE,1.0/USA..A/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions | 28,295                              | extraLarge | < 1.4 s        | < 2.8 s         | < 2.8 s         | < 5.6 s         |
| PT,EO_LIVE,1.0/USA..Q/?startPeriod=1940&endPeriod=2080&dimensionAtObservation=AllDimensions | 113,565                             | extraLarge | < 5.68 s       | < 11.35 s       | < 11.35 s       | < 22.7 s        |

  - **Smoke tests thresholds exports**
  
| Test group                | Thresholds |          |
|---------------------------|------------|--------- |
|                           | Previous   | Current  |
| csv_extraSmall            | 1s         | [+ 2s +] |
| csv_small                 | 1s         | [+ 2s +] |
| csv_medium                | 1s         | [+ 2s +] |
| csv_large                 | 1s         | [+ 2s +] |
| csv_extraLarge            | 5s         | [+ 10s +]|
|                           |            |          |
| csv_extraSmall_paginated  | 1s         | [+ 2s +] |
| csv_small_paginated       | 1s         | [+ 2s +] |
| csv_medium_paginated      | 1s         | [+ 2s +] |
| csv_large_paginated       | 1s         | [+ 2s +] |
| csv_extraLarge_paginated  | 5s         | [+ 10s +]|
|                           |            |          |
| json_extraSmall           | 1s         | [+ 2s +] |
| json_small                | 1s         | [+ 2s +] |
| json_medium               | 1s         | [+ 2s +] |
| json_large                | 1s         | [+ 2s +] |
| json_extraLarge           | 10s        | 10s      |
|                           |            |          |
| json_extraSmall_paginated | 1s         | [+ 2s +] |
| json_small_paginated      | 1s         | [+ 2s +] |
| json_medium_paginated     | 1s         | [+ 2s +] |
| json_large_paginated      | 1s         | [+ 2s +] |
| json_extraLarge_paginated | 5s         | [+ 10s +]|
|                           |            |          |
| xml_extraSmall            | 1s         | [+ 2s +] |
| xml_small                 | 1s         | [+ 2s +] |
| xml_medium                | 1s         | [+ 2s +] |
| xml_large                 | 1s         | [+ 2s +] |
| xml_extraLarge            | 10s        | 10s      |
|                           |            |          |
| xml_extraSmall_paginated  | 1s         | [+ 2s +] |
| xml_small_paginated       | 1s         | [+ 2s +] |
| xml_medium_paginated      | 1s         | [+ 2s +] |
| xml_large_paginated       | 1s         | [+ 2s +] |
| xml_extraLarge_paginated  | 5s         | [+ 10s +]|

## Current implementation

### Structure imports
All the implementations of the described test types can be found in this folder. 
- For structure imports, only one test type has been implemented: Smoke test
- This test type uses [a list of import requests](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-structure-imports.json) as a common source of the test cases. This list can be expanded to include more test cases.

#### Smoke test 
*[smoke-test-structure-imports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/smoke-test-structure-imports.js)*

The smoke test for structure imports is set to run structure imports requests against the nsi-ws.

The test consists in one virtual user that makes one import request for each test case found in the test-cases-structure-imports.json file. Each time it makes a request, it waits until it is finished, before submiting a new one.

After each request, it will get the status of the request as well as the time it took to process the input structure.

### Data imports
All the implementations of the described test types can be found in this folder. 
- For data imports, only two test types have been implemented: Smoke test and Load test
- Both test types use [a list of import requests](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-data-imports.json) as a common source of the test cases. This list can be expanded to include more test cases.

#### Smoke test 
*[smoke-test-data-imports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/smoke-test-data-imports.js)*

The smoke test for data imports is set to run data import requests against the transfer-service.

The test consists in one virtual user that makes one import request for each test case found in the test-cases-data-imports.json file. Each time it makes a request, it waits until it is finished, before submiting a new one.

Afterwards, the test will use the transfer-service's function /request/status to get the status of the transaction as well as the time it took to process the input data.

#### Load test
*[load-test-data-imports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/load-test-data-imports.js)*

The load test triggers multiple virtual users (VUs), one per test case in the test-cases-data-imports.json file. At the same time, all VUs will make a data import request.

Afterwards, the test will use the transfer-service's function /request/status to get the status of the transaction as well as the time it took to process the input data.

### Data extractions
All the implementations of the described test types can be found in this folder. 

- All test types use [a list of nsiws data/structure requests](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-exports.json) as a common source of the test cases. This list can be expanded to include more test cases.

#### Smoke test 
*[smoke-test-exports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/smoke-test-exports.js)*

The smoke test for data extraction is set to run data and structure requests aganist the nsi-ws.

The test consists in one virtual user that makes three requests (xml, json, csv) for each test case found in the test-cases-exports.json file. Each time it makes a request, it waits until it is finished, before requesting a new one.

#### Load test
*[load-test-exports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/load-test-exports.js)*

The load test triggers multiple VUs given by the *options* inside the script. Each VU will make a random request (based on the [test-cases.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-exports.json) file), for a random format (xml, json, csv)

#### Stress test
*[stress-test-exports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/stress-test-exports.js)*

It has the same implementation as the load test, only with a diferent set of *options* inside the script.
#### Spike test
*[spike-test-exports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/spike-test-exports.js)*

It has the same implementation as the load test, only with a diferent set of *options* inside the script.
#### Soak test
*[soak-test-exports.js](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/soak-test-exports.js)*

It has the same implementation as the load test, only with a diferent set of *options* inside the script.

## How to run the tests in different environments using Windows and Docker
The test types described above can be run in local environments, as well as remote deployments that are accessible by the machine running the performance tests.

### Pre-requisites
- A machine of VM with Windows, which will be used to run the tests.
- A deployment of the nsi-ws and the transfer service, which is accessible by the machine running the tests.
- Docker installed in the testing machine.
- This docker-compose installation example of the .stat-suite core components, deployed and accessible by the testing machine.

### Example.- Smoke-test structure imports nsi-ws
This example assumes that the docker-compose deployment of the .stat-suite core components is installed in the same computer where the k6 tests will be run.

> **Make sure that the [health page of the nsi-ws](http://localhost:81/health) is accessible.**
> The imports will use the stable dataspace

1.- Download the [dotstatsuite-quality-assurance](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance) gitlab repository into *C:/git/dotstatsuite-quality-assurance/*

2.- Place your command line in the folder *C:/git/dotstatsuite-quality-assurance/PerformanceTests* inside your local copy of dotstatsuite-quality-assurance gitlab repository
```
cd C:/git/dotstatsuite-quality-assurance/PerformanceTests/
```
3.- Run the smoke-test-structure-imports.js script inside a k6 docker container
```
docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-structure-imports.js
```
> **Make sure that you accept sharing with docker the volume C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources**

> When authentication is enabled in the nsi-ws, provide a value for the environment variables USERNAME and PASSWORD, as well as the path to the keycloak (ACCESS_TOKEN_URL) used by the authorisation service. For example *http://keycloak-staging-oecd.redpelicans.com/auth/realms/OECD/protocol/openid-connect/token* 

3.1.- Run the test using a different nsi-ws

If the nsi-ws is deployed in a different location, provide a value for the environment variable *NSIWS_HOSTNAME*

```
docker run --network="host" -e NSIWS_HOSTNAME="http://nsi-stable-qa.siscc.org" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-structure-imports.js
```

4.- Results

You should see the following output in your command console:
```
  ✓ checks..................................: 100.00% ✓ 2   ✗ 0
    data_received...........................: 8.3 kB  2.3 kB/s
    data_sent...............................: 1.4 MB  374 kB/s
    group_duration..........................: avg=764.26ms min=750.36ms med=764.26ms max=778.15ms p(90)=775.37ms p(95)=776.76ms
    http_req_blocked........................: avg=138.63µs min=10.4µs   med=181.7µs  max=223.8µs  p(90)=215.38µs p(95)=219.59µs
    http_req_connecting.....................: avg=105.83µs min=0s       med=140.2µs  max=177.3µs  p(90)=169.88µs p(95)=173.59µs
    http_req_duration.......................: avg=529.45ms min=62.16ms  med=749.93ms max=776.27ms p(90)=771.01ms p(95)=773.64ms
    ✓ { group:::Structure type multiple }...: avg=763.1ms  min=749.93ms med=763.1ms  max=776.27ms p(90)=773.64ms p(95)=774.96ms
    http_req_receiving......................: avg=298.96µs min=221.2µs  med=225.2µs  max=450.5µs  p(90)=405.44µs p(95)=427.96µs
    http_req_sending........................: avg=2.32ms   min=49.1µs   med=220µs    max=6.69ms   p(90)=5.39ms   p(95)=6.04ms
    http_req_tls_handshaking................: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s
    http_req_waiting........................: avg=526.83ms min=61.88ms  med=749.49ms max=769.13ms p(90)=765.2ms  p(95)=767.16ms
    http_reqs...............................: 3       0.829332/s
    iteration_duration......................: avg=1.79s    min=62.62ms  med=1.79s    max=3.53s    p(90)=3.18s    p(95)=3.35s
    iterations..............................: 1       0.276444/s
    vus.....................................: 1       min=1 max=1
    vus_max.................................: 1       min=1 max=1
```
[See more about interpreting the k6 results](https://k6.io/docs/getting-started/results-output)

### Example.- Smoke-test data imports transfer-service
This example assumes that the docker-compose deployment of the .stat-suite core components is installed in the same computer where the k6 tests will be run.

> **Make sure that the [health page of the transfer-service](http://localhost:93/health) is accessible.**
> **Make sure that the SDMX artefacts which will be used for the data uploads, exist in the correspondiding NSI-WS.**
> - The nsi-ws should have all [this sdmx artefacts](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/Structures)

1.- Download the [dotstatsuite-quality-assurance](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance) gitlab repository into *C:/git/dotstatsuite-quality-assurance/*

2.- Place your command line in the folder *C:/git/dotstatsuite-quality-assurance/PerformanceTests* inside your local copy of dotstatsuite-quality-assurance gitlab repository
```
cd C:/git/dotstatsuite-quality-assurance/PerformanceTests/
```
3.- Run the smoke-test-data-imports.js script inside a k6 docker container
```
docker run --network="host" -e TRANSFER_SERVICE_DATASPACE="stable" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-data-imports.js
```
> **Make sure that you accept sharing with docker the volume C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources**

> When authentication is enabled in the transfer service, provide a value for the environment variables USERNAME and PASSWORD, as well as the path to the keycloak (ACCESS_TOKEN_URL) used by the authorisation service. For example *http://keycloak-staging-oecd.redpelicans.com/auth/realms/OECD/protocol/openid-connect/token* 

3.1.- Run the test using a different transfer-service

If the transfer-service is deployed in a different location, provide a value for the environment variable *TRANSFER_SERVICE_HOSTNAME*

```
docker run --network="host" -e TRANSFER_SERVICE_DATASPACE="qa:stable" -e TRANSFER_SERVICE_HOSTNAME="http://transfer-qa.siscc.org" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-data-imports.js
```

4.- Results

You should see the following output in your command console:
```
  ✓ checks..........................: 100.00% ✓ 6   ✗ 0
  ✓ data_import_completed...........: 100.00% ✓ 6   ✗ 0
    data_import_time................: avg=16.85s   min=1.96s   med=13.92s  max=34.6s   p(90)=34.56s   p(95)=34.6s
    ✓ { datasetSize:extraSmall }....: avg=2.36s    min=1.96s   med=2.45s   max=2.67s   p(90)=2.62s    p(95)=2.64s
    ✓ { datasetSize:small }.........: avg=31.34s   min=25.18s  med=34.24s  max=34.6s   p(90)=34.53s   p(95)=34.56s
    ✓ { import_type:csv_medium }....: avg=25.18s   min=25.18s  med=25.18s  max=25.18s  p(90)=25.18s   p(95)=25.18s
    ✓ { import_type:csv_small }.....: avg=1.96s    min=1.96s   med=1.96s   max=1.96s   p(90)=1.96s    p(95)=1.96s
    ✓ { import_type:sdmx_medium }...: avg=34.6s    min=34.6s   med=34.6s   max=34.6s   p(90)=34.6s    p(95)=34.6s
    ✓ { import_type:sdmx_small }....: avg=2.45s    min=2.45s   med=2.45s   max=2.45s   p(90)=2.45s    p(95)=2.45s
    ✓ { import_type:xml_medium }....: avg=34.24s   min=34.24s  med=34.24s  max=34.24s  p(90)=34.24s   p(95)=34.24s
    ✓ { import_type:xml_small }.....: avg=2.67s    min=2.67s   med=2.67s   max=2.67s   p(90)=2.67s    p(95)=2.67s
    data_received...................: 12 kB   29 B/s
    data_sent.......................: 19 MB   49 kB/s
    http_req_blocked................: avg=137.61µs min=2µs     med=3.1µs   max=1.29ms  p(90)=238.18µs p(95)=660.87µs
    http_req_connecting.............: avg=95.71µs  min=0s      med=0s      max=884.7µs p(90)=180.76µs p(95)=462.71µs
    http_req_duration...............: avg=2.35s    min=3.95ms  med=99.37ms max=26.26s  p(90)=1.83s    p(95)=11.66s
    http_req_receiving..............: avg=85.93µs  min=51.7µs  med=89.3µs  max=151.7µs p(90)=107.1µs  p(95)=125.89µs
    http_req_sending................: avg=1.9ms    min=13.7µs  med=33.4µs  max=15ms    p(90)=4.49ms   p(95)=8.87ms
    http_req_tls_handshaking........: avg=0s       min=0s      med=0s      max=0s      p(90)=0s       p(95)=0s
    http_req_waiting................: avg=2.35s    min=3.88ms  med=99.22ms max=26.26s  p(90)=1.83s    p(95)=11.66s
    http_reqs.......................: 13      0.033274/s
    iteration_duration..............: avg=3m15s    min=99.86ms med=3m15s   max=6m30s   p(90)=5m51s    p(95)=6m11s
    iterations......................: 1       0.00256/s
    vus.............................: 1       min=1 max=1
    vus_max.........................: 1       min=1 max=1
```
[See more about interpreting the k6 results](https://k6.io/docs/getting-started/results-output)

 

### Example.- Smoke-test data extractions nsi-ws
This example assumes that the docker-compose deployment of the .stat-suite core components is installed in the same computer where the k6 tests will be run.

> **Make sure that the [health page of the nsi-ws](http://localhost:81/health) is accessible.**
> **Make sure that the SDMX artefacts which will be used for the extractions, exist in the correspondiding NSI-WS, and the data has been uploaded.**
> - The nsi-ws should have all [this sdmx artefacts](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/Structures) 
> - The nsi-ws should have all [all this data](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/Data) uploaded.

1.- Download the [dotstatsuite-quality-assurance](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance) gitlab repository into *C:/git/dotstatsuite-quality-assurance/*

2.- Place your command line in the folder *C:/git/dotstatsuite-quality-assurance/PerformanceTests* inside your local copy of the dotstatsuite-core-sdmxri-nsi-plugin gitlab repository
```
cd C:/git/dotstatsuite-quality-assurance/PerformanceTests/
```
3.- Run the smoke-test-exports.js script inside a k6 docker container
```
  docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-exports.js
```
> **Make sure that you accept sharing with docker the volume C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources**

3.1.- Run the test using a different nsi-ws

If the nsi-ws is deployed in a different location, provide a value for the environment variable *NSIWS_HOSTNAME*

```
docker run --network="host" -e NSIWS_HOSTNAME="http://nsi-stable-qa.siscc.org" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < smoke-test-exports.js
```

4.- Results

You should see the following output in your command console:
```
  ✓ checks.......................................: 100.00% ✓ 156 ✗ 0
    data_received................................: 74 MB   386 kB/s
    data_sent....................................: 36 kB   185 B/s
    group_duration...............................: avg=229.22ms min=76.41ms  med=214.33ms max=1.62s    p(90)=312.36ms p(95)=336.3ms
    http_req_blocked.............................: avg=6.46µs   min=2.9µs    med=3.5µs    max=250.69µs p(90)=5.04µs   p(95)=6.53µs
    http_req_connecting..........................: avg=2.02µs   min=0s       med=0s       max=206.5µs  p(90)=0s       p(95)=0s
    http_req_duration............................: avg=227.65ms min=3.76ms   med=213.75ms max=1.62s    p(90)=312.01ms p(95)=333.9ms
    ✓ { group:::Format csv::Size extraLarge }....: avg=1.44s    min=1.44s    med=1.44s    max=1.44s    p(90)=1.44s    p(95)=1.44s
    ✓ { group:::Format csv::Size extraSmall }....: avg=228.47ms min=195.61ms med=219.07ms max=312.16ms p(90)=282.22ms p(95)=304.41ms
    ✓ { group:::Format csv::Size large }.........: avg=319.59ms min=280.67ms med=303.79ms max=406.78ms p(90)=402.48ms p(95)=404.63ms
    ✓ { group:::Format csv::Size medium }........: avg=248.34ms min=213.42ms med=232.48ms max=329.23ms p(90)=315.1ms  p(95)=327.51ms
    ✓ { group:::Format csv::Size small }.........: avg=230.61ms min=207.17ms med=219.08ms max=341.08ms p(90)=267.97ms p(95)=304.2ms
    ✓ { group:::Format json::Size extraLarge }...: avg=1.62s    min=1.62s    med=1.62s    max=1.62s    p(90)=1.62s    p(95)=1.62s
    ✓ { group:::Format json::Size extraSmall }...: avg=224.18ms min=202.35ms med=210.05ms max=309ms    p(90)=282.37ms p(95)=308.42ms
    ✓ { group:::Format json::Size large }........: avg=347.43ms min=289.94ms med=328.31ms max=494.24ms p(90)=433.75ms p(95)=464ms
    ✓ { group:::Format json::Size medium }.......: avg=249.45ms min=221.85ms med=241.86ms max=315.82ms p(90)=294.92ms p(95)=313.27ms
    ✓ { group:::Format json::Size small }........: avg=224.56ms min=199.37ms med=213.41ms max=289.56ms p(90)=274.43ms p(95)=287.74ms
    ✓ { group:::Format xml::Size extraLarge }....: avg=1.18s    min=1.18s    med=1.18s    max=1.18s    p(90)=1.18s    p(95)=1.18s
    ✓ { group:::Format xml::Size extraSmall }....: avg=92.99ms  min=76.29ms  med=86.3ms   max=124.44ms p(90)=117.2ms  p(95)=122.04ms
    ✓ { group:::Format xml::Size large }.........: avg=180.37ms min=157.89ms med=170.78ms max=221.89ms p(90)=218.21ms p(95)=220.05ms
    ✓ { group:::Format xml::Size medium }........: avg=110.4ms  min=98.36ms  med=105.57ms max=137.44ms p(90)=131.73ms p(95)=137.38ms
    ✓ { group:::Format xml::Size small }.........: avg=94.74ms  min=83.79ms  med=90.78ms  max=125.15ms p(90)=115.07ms p(95)=122.95ms
    http_req_receiving...........................: avg=45.98ms  min=37µs     med=2.17ms   max=1.39s    p(90)=87.42ms  p(95)=111.79ms
    http_req_sending.............................: avg=37.1µs   min=12.8µs   med=32.09µs  max=189.5µs  p(90)=52.54µs  p(95)=71.55µs
    http_req_tls_handshaking.....................: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s
    http_req_waiting.............................: avg=181.62ms min=3.64ms   med=206.75ms max=339.69ms p(90)=276.96ms p(95)=300.37ms
    http_reqs....................................: 157     0.818505/s
    iteration_duration...........................: avg=1m35s    min=4.17ms   med=1m35s    max=3m11s    p(90)=2m52s    p(95)=3m2s
    iterations...................................: 1       0.005213/s
    vus..........................................: 1       min=1 max=1
    vus_max......................................: 1       min=1 max=1
```
[See more about interpreting the k6 results](https://k6.io/docs/getting-started/results-output)

### Runing the rest of the test for data extractions
The rest of the test found in this folder. They can be ran in the same way as the examples above, by simply indicating a different input js script to be ran. 
- **Load test**
```
docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < load-test-exports.js
```
- **Stress test**
```
docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < stress-test-exports.js
```
- **Spike test**
```
docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < spike-test-exports.js
```
- **Soak test**
```
docker run --network="host" -v "C:/git/dotstatsuite-quality-assurance/PerformanceTests/Resources:/Resources/" -i loadimpact/k6 run - < soak-test-exports.js
```
> The options in each test js script can be adjusted to the size of your testing environment, as well as to create a more realistic test scenario according your needs.

**[See more about the k6 documentation](https://k6.io/docs/)**

## Expected results
The following statistics on the different performance tests represent the expected results testing the .stat-suite in a single machine with **4 CPU cores and 8 GB of RAM with a solid state disk**. 

- **Smoke test**
```
  ✓ checks.........................................................: 100.00% ✓ 83   ✗ 0
    data_received..................................................: 79 MB   3.9 MB/s
    data_sent......................................................: 12 kB   588 B/s
    group_duration.................................................: avg=186.25ms min=5.14ms   med=120.13ms max=1.28s    p(90)=194.53ms p(95)=1s
    http_req_blocked...............................................: avg=30.36µs  min=2.7µs    med=3.5µs    max=284.7µs  p(90)=164.39µs p(95)=222.26µs
    http_req_connecting............................................: avg=19.34µs  min=0s       med=0s       max=223.4µs  p(90)=114.24µs p(95)=168.93µs
    http_req_duration..............................................: avg=146.99ms min=4.96ms   med=115.69ms max=1.28s    p(90)=191.26ms p(95)=203.61ms
    ✓ { datasetSize:extraSmall }...................................: avg=175.56ms min=136.32ms med=157.83ms max=255.97ms p(90)=253.01ms p(95)=254.49ms
    ✓ { datasetSize:small }........................................: avg=161.12ms min=96.98ms  med=115.22ms max=1.28s    p(90)=190.82ms p(95)=194.33ms
    ✓ { group:::Query type data::Format csv::Size extraSmall }.....: avg=134.75ms min=96.98ms  med=116.38ms max=191.33ms p(90)=189.43ms p(95)=191.2ms
    ✓ { group:::Query type data::Format csv::Size large }..........: avg=207.92ms min=176.98ms med=194.12ms max=252.68ms p(90)=240.97ms p(95)=246.82ms
    ✓ { group:::Query type data::Format csv::Size medium }.........: avg=136.54ms min=113.98ms med=124.58ms max=183.35ms p(90)=168.79ms p(95)=176.07ms
    ✓ { group:::Query type data::Format csv::Size small }..........: avg=107.73ms min=97.59ms  med=105.7ms  max=132.68ms p(90)=119.7ms  p(95)=126.19ms
    ✓ { group:::Query type data::Format json::Size extraLarge }....: avg=1.26s    min=1.25s    med=1.26s    max=1.28s    p(90)=1.28s    p(95)=1.28s
    ✓ { group:::Query type data::Format json::Size extraSmall }....: avg=111.33ms min=100.78ms med=105.5ms  max=146.14ms p(90)=126.18ms p(95)=136.16ms
    ✓ { group:::Query type data::Format json::Size large }.........: avg=207.31ms min=188.48ms med=192.4ms  max=255.97ms p(90)=237.49ms p(95)=246.73ms
    ✓ { group:::Query type data::Format json::Size medium }........: avg=131.51ms min=119.23ms med=123.02ms max=163.85ms p(90)=150.92ms p(95)=157.38ms
    ✓ { group:::Query type data::Format json::Size small }.........: avg=109.3ms  min=104.98ms med=110.23ms max=112.69ms p(90)=112.2ms  p(95)=112.45ms
    ✓ { group:::Query type data::Format xml::Size extraSmall }.....: avg=112.41ms min=97.94ms  med=104.39ms max=144.8ms  p(90)=138.86ms p(95)=141.83ms
    ✓ { group:::Query type data::Format xml::Size large }..........: avg=191.62ms min=176.61ms med=193.01ms max=205.24ms p(90)=202.8ms  p(95)=204.02ms
    ✓ { group:::Query type data::Format xml::Size medium }.........: avg=122.09ms min=97.17ms  med=121.86ms max=148.9ms  p(90)=128.88ms p(95)=138.89ms
    ✓ { group:::Query type data::Format xml::Size small }..........: avg=114.99ms min=110.68ms med=112.51ms max=126.85ms p(90)=121.15ms p(95)=124ms
    ✓ { group:::Query type structure::Struc type dataflow }........: avg=7.21ms   min=4.96ms   med=5.64ms   max=12.6ms   p(90)=10.64ms  p(95)=11.62ms
    ✓ { group:::Query type structure::Struc type datastructure }...: avg=12.06ms  min=8.19ms   med=11.73ms  max=16.58ms  p(90)=15.67ms  p(95)=16.13ms
    http_req_receiving.............................................: avg=42.52ms  min=32.8µs   med=1.2ms    max=1.18s    p(90)=75.83ms  p(95)=95.97ms
    http_req_sending...............................................: avg=22.26µs  min=11.1µs   med=13.85µs  max=98µs     p(90)=44.4µs   p(95)=60.29µs
    http_req_tls_handshaking.......................................: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s
    http_req_waiting...............................................: avg=104.44ms min=4.88ms   med=106.71ms max=191.22ms p(90)=143.41ms p(95)=159.02ms
    http_reqs......................................................: 84      4.199973/s
    iteration_duration.............................................: avg=1.22s    min=41.67ms  med=1.12s    max=2.28s    p(90)=2s       p(95)=2.01s
    iterations.....................................................: 76      3.799976/s
    vus............................................................: 9       min=1  max=9
    vus_max........................................................: 10      min=10 max=10
```
- **Load test**
```
  ✓ checks.........................................................: 99.59% ✓ 41405 ✗ 168
    data_received..................................................: 38 GB  16 MB/s
    data_sent......................................................: 6.4 MB 2.8 kB/s
    group_duration.................................................: avg=1.36s    min=4.19ms   med=633.99ms max=1m0s   p(90)=2.39s    p(95)=3.81s
    http_req_blocked...............................................: avg=5.35µs   min=1.5µs    med=3.7µs    max=5.49ms p(90)=5.4µs    p(95)=6.4µs
    http_req_connecting............................................: avg=714ns    min=0s       med=0s       max=5.43ms p(90)=0s       p(95)=0s
    http_req_duration..............................................: avg=1.33s    min=4.05ms   med=620.94ms max=1m0s   p(90)=2.36s    p(95)=3.74s
    ✓ { datasetSize:extraSmall }...................................: avg=1.6s     min=117.12ms med=917.25ms max=24.82s p(90)=3.15s    p(95)=4.58s
    ✓ { datasetSize:small }........................................: avg=1.39s    min=77.51ms  med=618.55ms max=1m0s   p(90)=2.34s    p(95)=3.74s
    ✓ { group:::Query type data::Format csv::Size extraLarge }.....: avg=15.02s   min=1.22s    med=10.78s   max=59.96s p(90)=32.5s    p(95)=43.17s
    ✓ { group:::Query type data::Format csv::Size extraSmall }.....: avg=822.32ms min=81.58ms  med=541.03ms max=16.22s p(90)=1.71s    p(95)=2.29s
    ✓ { group:::Query type data::Format csv::Size large }..........: avg=2.03s    min=168.36ms med=1.23s    max=24.98s p(90)=4.16s    p(95)=5.6s
    ✓ { group:::Query type data::Format csv::Size medium }.........: avg=1.19s    min=98.41ms  med=684.99ms max=24.22s p(90)=2.35s    p(95)=2.98s
    ✓ { group:::Query type data::Format csv::Size small }..........: avg=819.79ms min=78.82ms  med=543.82ms max=20.14s p(90)=1.7s     p(95)=2.29s
    ✗ { group:::Query type data::Format json::Size extraLarge }....: avg=14.73s   min=1.2s     med=10.45s   max=1m0s   p(90)=31.44s   p(95)=50.23s
    ✓ { group:::Query type data::Format json::Size extraSmall }....: avg=844.64ms min=77.51ms  med=541.56ms max=16.39s p(90)=1.66s    p(95)=2.26s
    ✓ { group:::Query type data::Format json::Size large }.........: avg=2.12s    min=167.86ms med=1.22s    max=24.55s p(90)=4.3s     p(95)=5.89s
    ✓ { group:::Query type data::Format json::Size medium }........: avg=1.23s    min=97.22ms  med=682.96ms max=24.06s p(90)=2.38s    p(95)=2.97s
    ✓ { group:::Query type data::Format json::Size small }.........: avg=835.87ms min=86.07ms  med=547.45ms max=15.77s p(90)=1.79s    p(95)=2.31s
    ✗ { group:::Query type data::Format xml::Size extraLarge }.....: avg=13.68s   min=1.16s    med=10.39s   max=1m0s   p(90)=27.91s   p(95)=35.22s
    ✓ { group:::Query type data::Format xml::Size extraSmall }.....: avg=861.02ms min=87.33ms  med=536.01ms max=20.2s  p(90)=1.84s    p(95)=2.35s
    ✓ { group:::Query type data::Format xml::Size large }..........: avg=2.12s    min=162.53ms med=1.24s    max=25.09s p(90)=4.14s    p(95)=5.73s
    ✓ { group:::Query type data::Format xml::Size medium }.........: avg=1.23s    min=96.7ms   med=684.02ms max=24.22s p(90)=2.3s     p(95)=2.94s
    ✓ { group:::Query type data::Format xml::Size small }..........: avg=858.28ms min=83ms     med=556.26ms max=15.57s p(90)=1.81s    p(95)=2.34s
    ✓ { group:::Query type structure::Struc type dataflow }........: avg=122.98ms min=4.05ms   med=19.52ms  max=15.05s p(90)=98.11ms  p(95)=169.03ms
    ✓ { group:::Query type structure::Struc type datastructure }...: avg=133.47ms min=7.58ms   med=43.14ms  max=14.62s p(90)=182.37ms p(95)=261.24ms
    http_req_receiving.............................................: avg=534.31ms min=10.7µs   med=6.25ms   max=58.77s p(90)=704.77ms p(95)=1.43s
    http_req_sending...............................................: avg=18.7µs   min=5.9µs    med=16µs     max=4.42ms p(90)=27.4µs   p(95)=33.1µs
    http_req_tls_handshaking.......................................: avg=0s       min=0s       med=0s       max=0s     p(90)=0s       p(95)=0s
    http_req_waiting...............................................: avg=803.98ms min=3.68ms   med=519.71ms max=22.35s p(90)=1.71s    p(95)=2.29s
    http_reqs......................................................: 41574  18.234209/s
    iteration_duration.............................................: avg=2.33s    min=433.14ms med=1.62s    max=1m1s   p(90)=3.36s    p(95)=4.75s
    iterations.....................................................: 41524  18.212279/s
    vus............................................................: 1      min=1   max=100
    vus_max........................................................: 100    min=100 max=100
```
- **Stress test**
```
  ✓ checks.........................................................: 99.50% ✓ 39953 ✗ 198
    data_received..................................................: 36 GB  16 MB/s
    data_sent......................................................: 6.2 MB 2.7 kB/s
    group_duration.................................................: avg=1.94s    min=3.65ms   med=1.16s   max=1m0s   p(90)=3.21s    p(95)=4.6s
    http_req_blocked...............................................: avg=5.25µs   min=1.6µs    med=3.6µs   max=4.91ms p(90)=5.2µs    p(95)=6.2µs
    http_req_connecting............................................: avg=741ns    min=0s       med=0s      max=4.86ms p(90)=0s       p(95)=0s
    http_req_duration..............................................: avg=1.89s    min=3.54ms   med=1.13s   max=1m0s   p(90)=3.17s    p(95)=4.53s
    ✓ { datasetSize:extraSmall }...................................: avg=2.27s    min=111.91ms med=1.78s   max=22.11s p(90)=4.33s    p(95)=5.86s
    ✓ { datasetSize:small }........................................: avg=1.99s    min=79.7ms   med=1.18s   max=1m0s   p(90)=2.97s    p(95)=4.37s
    ✗ { group:::Query type data::Format csv::Size extraLarge }.....: avg=21.81s   min=1.21s    med=23.85s  max=59.66s p(90)=38.13s   p(95)=48.64s
    ✓ { group:::Query type data::Format csv::Size extraSmall }.....: avg=1.25s    min=82.03ms  med=1.02s   max=16.61s p(90)=2.43s    p(95)=2.76s
    ✓ { group:::Query type data::Format csv::Size large }..........: avg=2.76s    min=168.31ms med=2.36s   max=20.98s p(90)=4.98s    p(95)=6.12s
    ✓ { group:::Query type data::Format csv::Size medium }.........: avg=1.63s    min=93.72ms  med=1.3s    max=20.34s p(90)=2.85s    p(95)=3.38s
    ✓ { group:::Query type data::Format csv::Size small }..........: avg=1.27s    min=82.5ms   med=1.05s   max=16.52s p(90)=2.43s    p(95)=2.8s
    ✗ { group:::Query type data::Format json::Size extraLarge }....: avg=21.4s    min=1.24s    med=23.12s  max=1m0s   p(90)=36.57s   p(95)=48.89s
    ✓ { group:::Query type data::Format json::Size extraSmall }....: avg=1.26s    min=79.7ms   med=1.04s   max=16.44s p(90)=2.43s    p(95)=2.79s
    ✓ { group:::Query type data::Format json::Size large }.........: avg=2.98s    min=167.67ms med=2.51s   max=22.11s p(90)=5.2s     p(95)=6.8s
    ✓ { group:::Query type data::Format json::Size medium }........: avg=1.66s    min=94.6ms   med=1.32s   max=18.51s p(90)=2.87s    p(95)=3.42s
    ✓ { group:::Query type data::Format json::Size small }.........: avg=1.24s    min=82.01ms  med=1.01s   max=16.77s p(90)=2.44s    p(95)=2.77s
    ✗ { group:::Query type data::Format xml::Size extraLarge }.....: avg=21.99s   min=1.2s     med=23.62s  max=59.25s p(90)=40.92s   p(95)=50.48s
    ✓ { group:::Query type data::Format xml::Size extraSmall }.....: avg=1.23s    min=80.48ms  med=1.02s   max=17.65s p(90)=2.39s    p(95)=2.73s
    ✓ { group:::Query type data::Format xml::Size large }..........: avg=2.93s    min=165.49ms med=2.46s   max=21.28s p(90)=5.2s     p(95)=6.36s
    ✓ { group:::Query type data::Format xml::Size medium }.........: avg=1.6s     min=92.09ms  med=1.28s   max=20.37s p(90)=2.83s    p(95)=3.33s
    ✓ { group:::Query type data::Format xml::Size small }..........: avg=1.22s    min=80.66ms  med=1s      max=16.32s p(90)=2.41s    p(95)=2.76s
    ✓ { group:::Query type structure::Struc type dataflow }........: avg=141.28ms min=3.54ms   med=37.75ms max=13.89s p(90)=165.65ms p(95)=288.34ms
    ✓ { group:::Query type structure::Struc type datastructure }...: avg=157.12ms min=7.17ms   med=82.4ms  max=14.41s p(90)=256.48ms p(95)=347.3ms
    http_req_receiving.............................................: avg=730.09ms min=10.9µs   med=5.69ms  max=58.19s p(90)=1.19s    p(95)=2.12s
    http_req_sending...............................................: avg=18.32µs  min=5.9µs    med=15.2µs  max=3.64ms p(90)=26.5µs   p(95)=32µs
    http_req_tls_handshaking.......................................: avg=0s       min=0s       med=0s      max=0s     p(90)=0s       p(95)=0s
    http_req_waiting...............................................: avg=1.16s    min=3.44ms   med=923.5ms max=17.77s p(90)=2.37s    p(95)=2.73s
    http_reqs......................................................: 40152  17.610525/s
    iteration_duration.............................................: avg=2.89s    min=44.06ms  med=2.13s   max=1m1s   p(90)=4.17s    p(95)=5.54s
    iterations.....................................................: 40105  17.589911/s
    vus............................................................: 1      min=1   max=100
    vus_max........................................................: 100    min=100 max=100
```
- **Spike test**
```
  ✗ checks.........................................................: 60.26% ✓ 3617  ✗ 2385
    data_received..................................................: 3.2 GB 6.9 MB/s
    data_sent......................................................: 937 kB 2.0 kB/s
    group_duration.................................................: avg=3.84s    min=4.2ms    med=2.03s    max=1m0s   p(90)=15.27s   p(95)=16.56s
    http_req_blocked...............................................: avg=11.74µs  min=1.4µs    med=3.4µs    max=6.17ms p(90)=5.2µs    p(95)=8.98µs
    http_req_connecting............................................: avg=6.17µs   min=0s       med=0s       max=6.1ms  p(90)=0s       p(95)=0s
    http_req_duration..............................................: avg=3.8s     min=3.79ms   med=1.91s    max=1m0s   p(90)=15.27s   p(95)=16.56s
    ✗ { datasetSize:extraSmall }...................................: avg=4.25s    min=120.11ms med=2.19s    max=22.67s p(90)=15.72s   p(95)=16.95s
    ✗ { datasetSize:small }........................................: avg=3.88s    min=78.46ms  med=2.07s    max=1m0s   p(90)=15.26s   p(95)=16.55s
    ✓ { group:::Query type data::Format csv::Size extraLarge }.....: avg=8.46s    min=1.21s    med=2.11s    max=1m0s   p(90)=18.9s    p(95)=35.97s
    ✗ { group:::Query type data::Format csv::Size extraSmall }.....: avg=3.09s    min=81.52ms  med=1.7s     max=20.56s p(90)=12.39s   p(95)=16s
    ✗ { group:::Query type data::Format csv::Size large }..........: avg=4.49s    min=171.43ms med=2.22s    max=22.84s p(90)=15.63s   p(95)=17.36s
    ✗ { group:::Query type data::Format csv::Size medium }.........: avg=4.36s    min=96.49ms  med=2.39s    max=22.79s p(90)=15.66s   p(95)=16.91s
    ✗ { group:::Query type data::Format csv::Size small }..........: avg=3.37s    min=78.46ms  med=2.04s    max=21.67s p(90)=14.62s   p(95)=16.23s
    ✗ { group:::Query type data::Format json::Size extraLarge }....: avg=12.77s   min=611.84ms med=2.66s    max=1m0s   p(90)=45.95s   p(95)=56.68s
    ✗ { group:::Query type data::Format json::Size extraSmall }....: avg=3.68s    min=85.56ms  med=2.08s    max=21.59s p(90)=15.16s   p(95)=16.65s
    ✗ { group:::Query type data::Format json::Size large }.........: avg=4.69s    min=168.72ms med=2.24s    max=22.71s p(90)=15.95s   p(95)=16.89s
    ✗ { group:::Query type data::Format json::Size medium }........: avg=4.07s    min=97.91ms  med=2.13s    max=22.26s p(90)=15.43s   p(95)=16.53s
    ✗ { group:::Query type data::Format json::Size small }.........: avg=3.7s     min=84.02ms  med=2.11s    max=21.12s p(90)=14.92s   p(95)=16.05s
    ✗ { group:::Query type data::Format xml::Size extraLarge }.....: avg=8.5s     min=202.26ms med=1.69s    max=1m0s   p(90)=30.88s   p(95)=43.19s
    ✗ { group:::Query type data::Format xml::Size extraSmall }.....: avg=3.5s     min=79.21ms  med=1.84s    max=21.18s p(90)=14.81s   p(95)=16.26s
    ✗ { group:::Query type data::Format xml::Size large }..........: avg=4.35s    min=167.39ms med=2.03s    max=23.06s p(90)=15.91s   p(95)=17.62s
    ✗ { group:::Query type data::Format xml::Size medium }.........: avg=3.89s    min=104.57ms med=2.13s    max=21.85s p(90)=15.47s   p(95)=16.63s
    ✗ { group:::Query type data::Format xml::Size small }..........: avg=3.5s     min=84.92ms  med=1.9s     max=22.86s p(90)=14.81s   p(95)=16.14s
    ✗ { group:::Query type structure::Struc type dataflow }........: avg=1.72s    min=4.1ms    med=82.39ms  max=20.12s p(90)=3.9s     p(95)=15.41s
    ✗ { group:::Query type structure::Struc type datastructure }...: avg=1.89s    min=7.74ms   med=226.51ms max=20.91s p(90)=1.99s    p(95)=15.48s
    http_req_receiving.............................................: avg=329.91ms min=9.5µs    med=69.3µs   max=57.83s p(90)=187.82ms p(95)=1.03s
    http_req_sending...............................................: avg=18.18µs  min=4.9µs    med=13.3µs   max=428µs  p(90)=30.8µs   p(95)=41.4µs
    http_req_tls_handshaking.......................................: avg=0s       min=0s       med=0s       max=0s     p(90)=0s       p(95)=0s
    http_req_waiting...............................................: avg=3.47s    min=3.62ms   med=1.83s    max=22.86s p(90)=14.89s   p(95)=16.19s
    http_reqs......................................................: 6003   13.049996/s
    iteration_duration.............................................: avg=4.81s    min=4.37ms   med=2.91s    max=1m1s   p(90)=16.27s   p(95)=17.57s
    iterations.....................................................: 5953   12.9413/s
    vus............................................................: 1      min=1   max=140
    vus_max........................................................: 140    min=140 max=140
```

- **Soak test**
```
    checks.....................: 99.89% ✓ 273225 ✗ 281
    data_received..............: 244 GB 17 MB/s
    data_sent..................: 42 MB  2.9 kB/s
    group_duration.............: avg=683.83ms min=3.66ms   med=339.11ms max=27.19s   p(90)=740.71ms p(95)=1.13s
    http_req_blocked...........: avg=4.59µs   min=1.4µs    med=3.4µs    max=135.9ms  p(90)=4.8µs    p(95)=5.6µs
    http_req_connecting........: avg=21ns     min=0s       med=0s       max=441µs    p(90)=0s       p(95)=0s
    http_req_duration..........: avg=670.42ms min=3.56ms   med=333.8ms  max=27.19s   p(90)=732.82ms p(95)=1.11s
    http_req_receiving.........: avg=262.21ms min=10.1µs   med=3.99ms   max=26.61s   p(90)=280.34ms p(95)=490.94ms
    http_req_sending...........: avg=18.03µs  min=5.8µs    med=13.8µs   max=166.42ms p(90)=27.3µs   p(95)=31.7µs
    http_req_tls_handshaking...: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s
    http_req_waiting...........: avg=408.19ms min=3.42ms   med=288.19ms max=20.79s   p(90)=505.78ms p(95)=599.37ms
    http_reqs..................: 273507 18.993541/s
    iteration_duration.........: avg=1.67s    min=449.22ms med=1.33s    max=28.19s   p(90)=1.73s    p(95)=2.12s
    iterations.................: 273478 18.991528/s
    vus........................: 1      min=1    max=32
    vus_max....................: 32     min=32   max=32
```

## How to add new test cases?
All the performance tests types, found in the *./PerformanceTests* folder, use a common file as the source of their test cases.  
The files can be found in the following locations:
- Structure imports
  - [./PerformanceTests/Resources/test-cases-structure-imports.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-structure-imports.json)
- Data imports
  - [./PerformanceTests/Resources/test-cases-data-imports.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-data-imports.json)
- Exports
  - [./PerformanceTests/Resources/test-cases-exports.json](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/tree/master/PerformanceTests/Resources/test-cases-exports.json)

To extend the test cases, simply add a new element to the json array containing the following:
- **Structure imports**
  - structureType.- The SDMX structure type: 
    - multiple.- when the xml file contains multiple structure types.
    - datastructure.
    - dataflow.
    - ....
    - ....
    - codelist.
  - structureFile.- The name of the import file (**The file must be uploaded into the folder** ./PerformanceTests/Resources/Structures/).

> **If you want to share your test cases for others to use them in their performance tests**, please provide the XML to create the SDMX structure as well as the data files.
> - Uplad the structure files to ./PerformanceTests/Resources/Structures/
> - Uplad the data files to ./PerformanceTests/Resources/Data/

Example 
```json
[
	{"structureType":"multiple",   "structureFile":"PT-DF_CPI-1.0.0-all.xml"},
	{"structureType":"multiple",   "structureFile":"PT-DF_WPI-1.0.0-all.xml"}	
]
```

- **Data imports**
> **Make sure that the sdmx structures have been previously loaded to the nsiws that will be used during the tests.**
  - format.- The format of the imput file (csv, xml, sdmx, excel)
  - size.- The amount of observations in the input file: 
    - extraSmall.- less than 1,000 obs.
    - small.- Between 1,000 and 500,000 obs.
    - medium.- Between 500,000 and 1 million obs.
    - large.- Between 1 million and 3 million obs.
    - extraLarge.- More than 3 million obs.
  - datasetSize.- The amount of observations already stored in the DB:
    - extraSmall.- less than 1,000 obs.
    - small.- Between 1,000 and 1 million obs.
    - medium.- Between 1 million and 3 million obs.
    - large.- Between 3 million and 10 million obs.
    - extraLarge.- More than 10 million obs.
  - dataFile.- The name of the import file (**The file must be uploaded into the folder** ./PerformanceTests/Resources/Data/).
  - sdmxSource.- (Only required for sdmx format) The url to a nsiws sdmx data source.
  - eddFile.- (Only required for excel format) The xml edd file (**The file must be uploaded into the folder** ./PerformanceTests/Resources/Data/).

> **If you want to share your test cases for others to use them in their performance tests**, please provide the XML to create the SDMX structure as well as the data files.
> - Uplad the structure files to ./PerformanceTests/Resources/Structures/
> - Uplad the data files to ./PerformanceTests/Resources/Data/

Example 
```json
[
	{"format":"csv",  "size":"small",  "datasetSize":"extraSmall", "dataFile":"PT-WPI-1.0.0-data.csv"},
	{"format":"csv",  "size":"medium", "datasetSize":"small", "dataFile":"PT-DF_CPI-1.0.0-data.csv.zip"},
	{"format":"xml",  "size":"small",  "datasetSize":"extraSmall",  "dataFile":"PT-DF_WPI-1.0.0-data.xml"},
	{"format":"xml",  "size":"medium", "datasetSize":"small", "dataFile":"PT-DF_CPI-1.0.0-data.xml.zip"},
	{"format":"sdmx", "size":"small",  "datasetSize":"extraSmall",  "sdmxSource":"http://nsi-stable-qa.siscc.org/rest/data/PT,DF_WPI,1.0.0/....../"},
	{"format":"sdmx", "size":"medium", "datasetSize":"small", "sdmxSource":"http://nsi-stable-qa.siscc.org/rest/data/PT,DF_CPI,1.0.0/......./"}
]
```
- **Data exports**
> **Make sure that the sdmx structures and data, have been previously loaded to the nsiws that will be used during the tests.**
  - queryType.- The type of nsiws sdmx query (data, structure).
  - structureType.- (Required only when queryType=structure) The type of sdmx structure:
    - agencyscheme, categoryscheme, categorisation, codelist, conceptscheme, contentconstraint, dataflow, datastructure, hierarchicalcodelist, metadataflow, metadatastructure, structureset.
  - responseSize.- The file **size of the response in XML** when requesting the given query to the nsiws:
    - extraSmall - (1-3KB)
    - small- (\~30-40KB)
    - medium - (\~400KB)
    - large - (\~2MB)
    - extraLarge - (20MB+) 
  - datasetSize.- The amount of observations already stored in the DB:
    - extraSmall.- less than 1,000 obs.
    - small.- Between 1,000 and 1 million obs.
    - medium.- Between 1 million and 3 million obs.
    - large.- Between 3 million and 10 million obs.
    - extraLarge.- More than 10 million obs.
  - query.- The nsiws data query, withouth the base url (/rest/data/{agency}/{dataflow}/{version}/{filtered dimensions}/?{extra filtering parameters}).

> **If you want to share your test cases for others to use them in their performance tests**, please provide the XML to create the SDMX structure as well as the data files. 
> - Upload the structure files to ./PerformanceTests/Resources/Structures/
> - Upload the data files to ./PerformanceTests/Resources/Data/

Example 
```json
[
    {"queryType":"data", "responseSize": "extraSmall", "datasetSize":"extraSmall", "query": "/rest/data/PT,DF_WPI,1.0.0/2.THRPEB.1.TOT.10.3.Q/PT?startPeriod=2017-Q2&endPeriod=2018-Q2"},
    {"queryType":"data", "responseSize": "small", "datasetSize":"small", "query": "/rest/data/PT,DF_CPI,1.0.0/.10001...Q/?startPeriod=2017-Q2&endPeriod=2018-Q2"},
    {"queryType":"data", "responseSize": "medium", "datasetSize":"small", "query": "/rest/data/PT,DF_CPI,1.0.0/1..10.6+7+8.Q/?startPeriod=2017-Q2&endPeriod=2018-Q2"},
    {"queryType":"data", "responseSize": "large", "datasetSize":"extraSmall", "query": "/rest/data/PT,DF_WPI,1.0.0/all/PT?startPeriod=1999&endPeriod=2017"},
    {"queryType":"data", "responseSize": "extraLarge", "datasetSize":"small", "query": "/rest/data/PT,DF_CPI,1.0.0/all/?startPeriod=2010"},
    {"queryType":"structure", "structureType":"datastructure", "query": "/rest/datastructure/pt/DS_CPI/1.0.0"},
    {"queryType":"structure", "structureType":"dataflow", "query": "/rest/dataflow/PT/DF_CPI/1.0.0/"},
	...
]
```

